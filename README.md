The Java program will be able to scan a text file and return: 

(1)total amount to words.

(2)total different words.

(3)total count for each word. 

It will have GUI interface in where the user can input a specified text (.txt) document, along with a scan button to begin scanning of document. 
Once the program is started, the user will input file via the add document button. The program will scan and form a text document with the results.