    /*Project Text Scanner
	*This program will scan a text document (.TXT) and produce the: Total word count, 
	*all different words, and amount of different words each.
	*A gui will be prompted and require text to be uploaded with a "scan" button
	*ready to scan file.
	* @author Vinod Ramdin
	*Author:Vinod Ramdin 10/10/16
	*Compilers I. Instructor. Erik S*/
	
	import java.awt.event.ActionEvent;
	import java.awt.event.ActionListener;
	import java.io.BufferedReader;
	import java.io.File;
	import java.io.FileInputStream;
	import java.io.FileNotFoundException;
	import java.io.FileReader;
	import java.io.IOException;
	import java.util.ArrayList;
	import java.util.Scanner;

	import javax.swing.JButton;
	import javax.swing.JFileChooser;
	import javax.swing.JFrame;
	import javax.swing.JLabel;
	import javax.swing.JTextField;
	import javax.swing.filechooser.FileNameExtensionFilter;

	
	public class Textreader extends JFrame {

	JButton scanButton = new JButton( "Scan");
	JTextField jtextField1 = new JTextField( );
	JLabel addtextLabel = new JLabel( "First, click below to add text file: ");
	JLabel scanLabel = new JLabel( "Then click scan to begin scanning:");
	JLabel answer = new JLabel ( "" );
	JLabel results = new JLabel ("Results:");
	JLabel different = new JLabel ("Total count of different words:");
	JLabel wordcount = new JLabel ("Total count of words scanned:");
	JButton fileButton = new JButton( "Add text file:");

		public Textreader() {
		this.setTitle("Text Document Scanner 1.0");  //Sets the name of title in window
		this.setBounds( 725, 400, 500, 500);  // Sets the size of the window  (x,y,width,height) x and y are position for where window opens
		this.getContentPane().setLayout(null);
		this.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE);

		this.scanButton.setBounds(200, 150, 150, 30); //Scan button
		this.getContentPane().add( scanButton);
		this.scanButton.addActionListener( new ButtonListener());

		this.jtextField1.setBounds( 370, 50, 100, 30);  // text box
		this.getContentPane().add( jtextField1);

		this.addtextLabel.setBounds(20, 11, 300, 30);// click below text label
		this.getContentPane().add( addtextLabel);

		this.scanLabel.setBounds(20, 100, 300, 30); //then click scan label
		this.getContentPane().add( scanLabel);

		this.answer.setBounds(20, 100, 200, 300);  //moves the answer when pushing scan
		this.getContentPane().add( answer);

		this.fileButton.setBounds(200, 50, 150, 30); //add text file button
		this.getContentPane().add(fileButton);
		this.fileButton.addActionListener( new FileButtonListener());

		 this.results.setBounds(20, 250, 300, 30);
		 this.getContentPane().add(results);

		 this.different.setBounds(20, 350, 350, 30);
		 this.getContentPane().add(different);

		 this.wordcount.setBounds(20, 180, 200, 300);
		 this.getContentPane().add(wordcount);
	}


	private void theButtonHasBeenPushed() {
		String text = jtextField1.getText();
		ArrayList<Storage> storage = new ArrayList<Storage>();
		try {
			FileReader reader  = new FileReader(text);
			BufferedReader buff = new BufferedReader(reader);// passing in file reader
			String line ; //one line being read in
			while ((line = buff.readLine())!= null){
				line = line.toUpperCase();
				line = line.replaceAll("[^A-Z]"," ");
				String [] words=line.split(" "); //array of words	
				for(String word: words){
					if(word.length()>0){
						boolean found = false;
						for(Storage stor: storage){
							if(stor.getWord().equals(word)){
								stor.incrementcount();
								break;
							}
						}
						if(!found){
							Storage stor =new Storage(word);
							storage.add(stor);
						}
					}
				}

			}
			System.out.println("Total word count: " + Storage.getTotalCount());
			System.out.println("Total amount of different words: " + storage.size());
			for(Storage stor:storage){
				System.out.println("Count for each different word: " + stor.getWord()+" : " + stor.getCount());
			}
			System.out.flush();
			System.exit(1);

		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch(IOException e1){
			e1.printStackTrace();
		}
	}
	private class ButtonListener implements ActionListener {

		@Override
		public void actionPerformed( ActionEvent e) {
			Textreader.this.theButtonHasBeenPushed();
		}
	}
	public class FileButtonListener implements ActionListener {
		@Override
		public void actionPerformed( ActionEvent e) {

			JFileChooser chooser = new JFileChooser();
			chooser.setFileFilter( new FileNameExtensionFilter("TEXT FILES", "txt", "text"));

			int chooserSuccess = chooser.showOpenDialog( null);//opens file dialog box

			if( chooserSuccess == JFileChooser.APPROVE_OPTION) {
				File chosenFile = chooser.getSelectedFile(); //returns the selected file
				// Pass this file to your function
				String filename = chosenFile.getAbsolutePath();
				jtextField1.setText(filename);	 
			}
		}
	}
}
	/*Constructor with setters and getters
	* @Vinod Ramdin
	**/
	public class Storage {
		private static int totalCount =0;//one instance 
		private String word;
		private int count;

	Storage(String word){
		this.word= word;
		count = 1; 
		totalCount++;
	}
	
	public void incrementcount(){
		count++;
		totalCount++;
	}
	
    public String getWord(){
    	return word;
    	
    }
    
    public int getCount() {
    	return count;
    }
    
    public static int getTotalCount(){
    	return totalCount;
    }
}
	/**
	*Main 
	*@param args the command line arguments
	* Author:Vinod Ramdin 10/10/16
	*Compilers I. Instructor. Erik S.
	*/
	
public class Main {

 public static void main(String[] args)  {
	        // TODO code application logic here
	        Textreader tr = new Textreader();
	        tr.setVisible(true);  
	    }}
	

